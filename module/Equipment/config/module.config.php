<?php

return array (
    //'view_manager' => array(
    //    'template_map'              => include __DIR__  .'/../template_map.php',
    //    'template_path_stack'       => array(
    //        __DIR__ . '/../view',
    //    ),
    //),
    'service_manager' => array(
        'invokables' => array(
            'Equipment\Service\AddMessagesToMessengerCommand' => 'Equipment\Application\Service\AddMessagesToMessengerCommand',
            'Equipment\Service\ReadAllFilesCommand' => 'Equipment\Application\Service\ReadAllFilesCommand',
        ),
        'factories' => array(
            'Equipment\Service\FileService' => 'Equipment\Application\Service\Factory\FileServiceFactory',
        ),
    ),
    'view_helpers'    => array(
        'invokables' => array(
            'BootstrapForm' => 'Equipment\View\Helper\BootstrapForm',
        ),
    ),
);
