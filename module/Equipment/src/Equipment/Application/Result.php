<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Equipment\Application;

/**
 * Description of Result
 *
 * @author cparadies
 */
class Result
{
    /**
     * @var boolean is valid or not
     */
    protected $valid;
    /**
     * @var string Message of the result
     */
    protected $message;
    /**
     * @var int the type of the message
     */
    protected $messageType;
    /**
     * @var mixed the data for the result
     */
    protected $data;
    /**
     * @var const default message
     */
    const DEFAULT_MESSAGE = 0;
    /**
     * @var const success message
     */
    const SUCCESS_MESSAGE = 1;
    /**
     * @var const info message
     */
    const INFO_MESSAGE    = 2;
    /**
     * @var const warning message
     */
    const WARNING_MESSAGE = 3;
    /**
     * @var const error message
     */
    const ERROR_MESSAGE   = 4;
    
    public function isValid()
    {
        return $this->valid;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getMessageType()
    {
        return $this->messageType;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setValid($valid)
    {
        $this->valid = $valid;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function setMessageType($messageType)
    {
        $this->messageType = $messageType;
    }

    public function setData($data)
    {
        $this->data = $data;
    }
    
    public static function getResult(
        $message,
        $messageType,
        $data,
        $valid
    ) {
        $result = new Result();
        $result->setMessage($message);
        $result->setMessageType($messageType);
        $result->setValid($valid);
        $result->setData($data);
        
        return $result;
    }
}
