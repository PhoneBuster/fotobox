<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Equipment\Application\Service;

use Equipment\Application\Exception\InvalidArgumentException;
use Equipment\Application\Exception\NullpointerException;
use Equipment\Application\Result;
use Equipment\Application\Service\Interfaces\CommandInterface;
use Zend\Mvc\Controller\Plugin\FlashMessenger;

/**
 * Description of AddMessagesToMessengerCommand
 *
 * @author cparadies
 */
class AddMessagesToMessengerCommand implements CommandInterface
{
    /**
     * @var FlashMessenger
     */
    protected $flashMessenger     = null;
    /**
     * @var array
     */
    protected $messages           = null;
    protected $commonMessageType  = Result::DEFAULT_MESSAGE;
    
    public function execute()
    {
        if (is_null($this->flashMessenger)) {
            throw new NullpointerException(
                'FlashMessenger is null. Set it before you execute this command'
            );
        }
        
        if (is_null($this->messages)) {
            throw new NullpointerException(
                'Messages are null. Set it before you execute this command'
            );
        }
        
        return $this->addMessagesToMessenger();
    }
    
    protected function addMessagesToMessenger()
    {
        if (!is_array($this->messages)) {
            throw new InvalidArgumentException(
                'Messages have to be an array. ' . get_class($this->messages) . ' given'
            );
        }
        
        $this->extractMessage($this->messages);
        return $this->getResult($this->flashMessenger, true, Result::INFO_MESSAGE);
    }
    
    protected function getResult($data = null, $valid = true, $messageType = Result::DEFAULT_MESSAGE)
    {
        $result = new Result();
        
        $result->setValid($valid);
        $result->setData($data);
        $result->setMessage('Messages added to messenger');
        $result->setMessageType($messageType);
        
        return $result;
    }
    
    protected function extractMessage(array $messages)
    {
        foreach ($messages as $message) {
            if (is_array($message)) {
                $this->extractMessage($message);
            } else {
                if (is_string($message)) {
                    $this->addMessage($messages, $message);
                }
            }
        }
    }
    
    protected function addMessage(array $messages, $message)
    {
        if (\array_key_exists('messagetype', $messages)) {
            $this->commonMessageType = $messages['messagetype'];
        }
        
        switch ($this->commonMessageType) {
            case Result::DEFAULT_MESSAGE:
                $this->flashMessenger->addMessage($message);
                return;
            case Result::SUCCESS_MESSAGE:
                $this->flashMessenger->addSuccessMessage($message);
                return;
            case Result::INFO_MESSAGE:
                $this->flashMessenger->addInfoMessage($message);
                return;
            case Result::WARNING_MESSAGE:
                $this->flashMessenger->addWarningMessage($message);
                return;
            case Result::ERROR_MESSAGE:
                $this->flashMessenger->addErrorMessage($message);
                return;
            default:
                $this->flashMessenger->addMessage($message);
                return;
        }
    }
    
    public function getFlashMessenger()
    {
        return $this->flashMessenger;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    public function setFlashMessenger(FlashMessenger $flashMessenger)
    {
        $this->flashMessenger = $flashMessenger;
        return $this;
    }

    /**
     * Set Messages
     * 
     * @param array $messages
     * @return \Equipment\Application\Service\AddMessagesToMessengerCommand
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;
        return $this;
    }
    
    public function getCommonMessageType()
    {
        return $this->commonMessageType;
    }

    public function setCommonMessageType($commonMessageType)
    {
        $this->commonMessageType = $commonMessageType;
        return $this;
    }
}
