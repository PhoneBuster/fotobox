<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Equipment\Application\Service\Factory;

use Equipment\Application\Service\FileService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Description of FileServiceFactory
 *
 * @author cparadies
 */
class FileServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $readAllFilesCommand = $serviceLocator->get('Equipment\Service\ReadAllFilesCommand');
        
        $fileService         = new FileService($readAllFilesCommand);
        
        return $fileService;
    }
}
