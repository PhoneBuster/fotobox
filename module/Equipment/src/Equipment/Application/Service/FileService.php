<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Equipment\Application\Service;

/**
 * Description of FileService
 *
 * @author cparadies
 */
class FileService
{
    /**
     * @var ReadAllFilesCommand
     */
    private $readAllFilesCommand;
    
    public function __construct(ReadAllFilesCommand $readAllFilesCommand)
    {
        $this->readAllFilesCommand = $readAllFilesCommand;
    }
    
    public function getAllFilesFromFolder($pathToRead)
    {
        $this->readAllFilesCommand->setPathToRead($pathToRead);
        $result = $this->readAllFilesCommand->execute();
        return $result;
    }
}
