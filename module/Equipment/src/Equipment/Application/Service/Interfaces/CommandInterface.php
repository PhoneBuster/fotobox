<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Equipment\Application\Service\Interfaces;

use Equipment\Application\Result;

/**
 *
 * @author cparadies
 */
interface CommandInterface
{
    /**
     * @return Result
     */
    public function execute();
}
