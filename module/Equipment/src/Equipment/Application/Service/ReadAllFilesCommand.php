<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Equipment\Application\Service;

use Equipment\Application\Execption\NullpointerException;
use Equipment\Application\Result;
use Equipment\Application\Service\Interfaces\CommandInterface;

/**
 * Description of ReadAllFilesCommand
 *
 * @author cparadies
 */
class ReadAllFilesCommand implements CommandInterface
{
    private $pathToRead = null;
    
    public function __construct()
    {
        //Default
    }
    
    /**
     *
     * @return Result
     * @throws NullpointerException
     */
    public function execute()
    {
        if (is_null($this->pathToRead)) {
            throw new NullpointerException(
                'pathtoread is null. Set it before executing this command!'
            );
        }
        
        return $this->readAllFilesFromPath();
    }
    
    private function readAllFilesFromPath()
    {
        $allFiles = glob($this->pathToRead . "/*.*");
        if (!is_array($allFiles)) {
            return Result::getResult(
                'No Files available',
                Result::INFO_MESSAGE,
                array(),
                true
            );
        }
        
        return Result::getResult(
            'All files from ' . $this->pathToRead,
            Result::SUCCESS_MESSAGE,
            $allFiles,
            true
        );
    }
    
    public function setPathToRead($pathToRead)
    {
        $this->pathToRead = $pathToRead;
    }
}
