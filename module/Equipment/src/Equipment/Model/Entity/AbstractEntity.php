<?php
/**
 * Zend Framework Schulung
 *
 * @package    Core
 * @author     Ralf Eggert <r.eggert@travello.de>
 * @copyright  Ralf Eggert <r.eggert@travello.de>
 * @link       http://www.zendframeworkschulung.de/
 */

/**
 * namespace definition and usage
 */
namespace Equipment\Model\Entity;

use Core\Model\Value\AbstractValue;
use Zend\Filter\StaticFilter;
use Zend\Stdlib\ArraySerializableInterface;

/**
 * Class AbstractEntity
 *
 * @package Core\Model\Entity
 */
abstract class AbstractEntity implements ArraySerializableInterface
{
    /**
     * Exchange internal values from provided array
     *
     * @param  array $array
     *
     * @return void
     */
    public function exchangeArray(array $array)
    {
        foreach ($array as $key => $value) {
            $setMethod = 'set' . StaticFilter::execute(
                $key,
                'WordUnderscoreToCamelCase'
            );

            if (method_exists($this, $setMethod)) {
                $this->$setMethod($value);
            }
        }
    }

    /**
     * Return an array representation of the object
     *
     * @return array
     */
    public function getArrayCopy()
    {
        $array = array();

        foreach (get_object_vars($this) as $key => $value) {
            $getMethod = 'get' . StaticFilter::execute(
                $key,
                'WordUnderscoreToCamelCase'
            );

            $key = StaticFilter::execute($key, 'WordCamelCaseToUnderscore');
            $key = StaticFilter::execute($key, 'StringToLower');

            if (method_exists($this, $getMethod)) {
                $array[$key] = $this->$getMethod();
            }
        }

        return $array;
    }
}
