<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Equipment\Model\Value;

/**
 * Description of Cost
 *
 * @author cparadies
 */
class Cost
{
    protected $cost;
    
    public function __construct($cost)
    {
        $this->cost = $cost;
    }
    
    public function getCost()
    {
        return $this->cost;
    }

    public function setCost($cost)
    {
        $this->cost = $cost;
        return $this;
    }
}
