<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Equipment\Model\Value;

use Zend\Validator\Hostname;
use Zend\Validator\StaticValidator;

/**
 * Description of EmailAddress
 *
 * @author cparadies
 */
class EmailAddress
{
    protected $email;
    
    public function __construct($email)
    {
        $this->email = $email;
        $this->validate();
    }
    
    public function validate()
    {
        $result = StaticValidator::execute(
            $this->email,
            'EmailAddress',
            array('allow' => Hostname::ALLOW_DNS)
        );
        
        if ($result === false) {
            $this->email = 'No valid email';
        }
    }
    
    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        $this->validate();
        return $this;
    }
}
