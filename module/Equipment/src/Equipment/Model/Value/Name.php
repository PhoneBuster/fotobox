<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Equipment\Model\Value;

/**
 * Description of Name
 *
 * @author cparadies
 */
class Name
{
    protected $name;
    protected $surname;
    
    public function __construct($name, $surname = "")
    {
        $this->name = $name;
        $this->surname = $surname;
    }
    
    public function getName()
    {
        return $this->name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }
    
    public function getFullName()
    {
        if ($this->surname != "" && $this->name != "") {
            $fullName = $this->name . ' ' . $this->surname;
        } else {
            if ($this->name != "") {
                $fullname = $this->name;
            }
            if ($this->surname != "") {
                $fullname = $this->surname;
            }
        }
        
        return $fullName;
    }
}
