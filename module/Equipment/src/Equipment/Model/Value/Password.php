<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Equipment\Model\Value;

/**
 * Description of Password
 *
 * @author cparadies
 */
class Password
{
    protected $password;
    
    public function __construct($password)
    {
        $this->password = $password;
    }
    
    public function getPassword()
    {
        return $this->password;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }
}
