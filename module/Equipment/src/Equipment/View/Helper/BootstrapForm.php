<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Equipment\View\Helper;

use Zend\Form\Element;
use Zend\Form\Element\Csrf;
use Zend\Form\Form;
use Zend\View\Helper\AbstractHelper;
use Zend\View\Renderer\PhpRenderer;

/**
 * Description of BoostrapForm
 *
 * @author cparadies
 */
class BootstrapForm extends AbstractHelper {
    public function __construct()
    {
        //Default Constructor
    }
    
    public function __invoke(Form $form)
    {
        $this->renderForm($form);
    }
    
    protected function renderForm(Form $form)
    {
        /* @var $phpRenderer PhpRenderer */
        $phpRenderer = $this->getView()->getEngine();
        
        echo $phpRenderer->form()->openTag($form);
        /* @var $element Element */
        foreach($form AS $element) {
            if ($element instanceof Csrf) {
                $this->renderOneColumnElement($element, $phpRenderer);
                continue;
            }
            if ($element instanceof Element\Submit) {
                $this->renderOneColumnElement($element, $phpRenderer);
                continue;
            }
            
            $this->renderStandard($element, $phpRenderer);
        }
        
        echo $phpRenderer->form()->closeTag($form);
    }
    
    protected function renderStandard(Element $element, PhpRenderer $phpRenderer)
    {
        echo '<div class="form-group">';
        $this->renderLabel($element);
        echo '  <div class="col-sm-10">';
        echo $phpRenderer->formElement($element);
        echo '  </div>';
        echo '</div>';        
    }
    
    protected function renderLabel(Element $element)
    {
        if ($element->getLabel() != "") {
            echo '<label for="' . $element->getName() . '" ';
            echo 'class="col-sm-2 control-label">' . $element->getLabel();
            echo '</label>';
        }
    }
    
    protected function renderOneColumnElement(Element $element, PhpRenderer $phpRenderer)
    {
        echo '<div class="form-group">';
        echo '  <div class="col-sm-offset-2 col-sm-10">';
        echo $phpRenderer->formElement($element);
        echo '  </div>';
        echo '</div>';
    }
}
