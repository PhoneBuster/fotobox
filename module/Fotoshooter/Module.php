<?php
namespace Fotoshooter;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);
              
        // add application listener
        //$eventManager->attachAggregate(new ApplicationListener());
    }

    public function getConfig()
    {
        $moduleconfig      = include __DIR__ . '/config/module.config.php';
        $fotoshooterconfig = include __DIR__ . '/config/fotoshooter.config.php';
        
        $config = array_merge($moduleconfig, $fotoshooterconfig);
        return $config;
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __NAMESPACE__ => __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }
}
