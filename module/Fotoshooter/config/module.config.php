<?php
if (!defined('FOTOSHOOTER_ROOT')) {
    define('FOTOSHOOTER_ROOT', realpath(__DIR__ . '/..'));
}

return array(
    'router' => array(
        'routes' => array(
            'shooting' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/shoot/:picture',
                    'defaults' => array(
                        'controller' => 'Fotoshooter\Controller\Shoot',
                        'action'     => 'shoot',
                    ),
                ),
            ),
            'backgroundselect' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/background/select',
                    'defaults' => array(
                        'controller' => 'Fotoshooter\Controller\Shoot',
                        'action'     => 'bgselect',
                    ),
                ),
            ),
            'showimage' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/image/show',
                    'defaults' => array(
                        'controller' => 'Fotoshooter\Controller\Shoot',
                        'action'     => 'show',
                    ),
                ),
            ),
            'imagesaved' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/image/saved',
                    'defaults' => array(
                        'controller' => 'Fotoshooter\Controller\Shoot',
                        'action'     => 'saved',
                    ),
                ),
            ),
            'backgroundcompose' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/background/compose/:id',
                    'defaults' => array(
                        'controller' => 'Fotoshooter\Controller\Shoot',
                        'action'     => 'compose',
                    ),
                ),
            ),
            'waitforbutton' => array(
                'type' => 'Segment',
                'options' => array(
                    'route'    => '/image/wait/:picture',
                    'defaults' => array(
                        'controller' => 'Fotoshooter\Controller\Shoot',
                        'action'     => 'wait',
                    ),
                ),
            ),
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map'              => include __DIR__  .'/../template_map.php',
        'template_path_stack'       => array(
            __DIR__ . '/../view',
        ),
    ),
    'service_manager' => array(
        'factories' => array(
            'Fotoshooter\Repository\Background' => 'Fotoshooter\Model\Repository\Factory\BackgroundimageRepositoryFactory',
            'Fotoshooter\FileGateway\Background' => 'Fotoshooter\Model\FileGateway\Factory\BackgroundFileGatewayFactory',
            'Fotoshooter\Service\FileInfo' => 'Fotoshooter\Service\Factory\FileInfoServiceFactory',
            'Fotoshooter\Service\ReadAllFilesCommand' => 'Fotoshooter\Service\Factory\ReadAllFilesCommandFactory',
            'Fotoshooter\Service\ImageComposer' => 'Fotoshooter\Service\Factory\ImageComposerServiceFactory',
        ),
        'invokables' => array(
            'Fotoshooter\Service\GetFileSizeCommand' => 'Fotoshooter\Service\GetFileSizeCommand',
            'Fotoshooter\Service\ReadFileCommand' => 'Fotoshooter\Service\ReadFileCommand',
            'Fotoshooter\Service\SaveComposedImageCommand' => 'Fotoshooter\Service\SaveComposedImageCommand',
            'Fotoshooter\Service\DeleteComposedImageCommand' => 'Fotoshooter\Service\DeleteComposedImageCommand',
        ),
    ),
    'controllers' => array(
        'factories' => array(
            'Fotoshooter\Controller\Shoot' => 'Fotoshooter\Controller\Factory\ShootControllerFactory',
        ),
    ),
    'view_helpers'    => array(
        'invokables' => array(
            'BackgroundGallery' => 'Fotoshooter\View\Helper\BackgroundGallery',
        ),
    ),
);
