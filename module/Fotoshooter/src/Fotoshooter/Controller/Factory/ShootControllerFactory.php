<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Fotoshooter\Controller\Factory;

use Fotoshooter\Controller\ShootController;
use Zend\Config\Config;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Description of ShootControllerFactory
 *
 * @author cparadies
 */
class ShootControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $controllerManager)
    {
        $serviceLocator = $controllerManager->getServiceLocator();
        $formElementManager        = $serviceLocator->get('FormElementManager');
        $backgroundimageRepository = $serviceLocator->get('Fotoshooter\Repository\Background');
        $imageComposerService      = $serviceLocator->get('Fotoshooter\Service\ImageComposer');
        $deleteForm                = $formElementManager->get('Fotoshooter\DeleteForm');
        $config                    = $serviceLocator->get('Config');

        $controller = new ShootController(
            $backgroundimageRepository,
            $imageComposerService,
            $deleteForm,
            new Config($config['dev_environment']),
            new Config($config['fotoshooter'])
        );
        
        return $controller;
    }
}
