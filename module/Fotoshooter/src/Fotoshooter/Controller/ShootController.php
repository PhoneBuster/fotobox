<?php

/*
 * $Id$
 * 
 * @copyright celexon Germany GmbH & Co. KG, Emsdetten
 * 
 */

namespace Fotoshooter\Controller;

use Fotoshooter\Model\Form\DeleteForm;
use Fotoshooter\Model\Repository\BackgroundimageRepository;
use Fotoshooter\Service\ImageComposerService;
use Zend\Config\Config;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Description of ShootController
 *
 * @author cparadies
 */
class ShootController extends AbstractActionController
{
    /**
     * @var BackgroundimageRepository
     */
    private $backgroundimageRepository;
    /**
     * @var ImageComposerService
     */
    private $imageComposerService;
    /**
     * @var DeleteForm
     */
    private $deleteForm;
    /**
     * @var Config
     */
    private $developmentconfig;
    /**
     * @var Config
     */
    private $fotoshooterconfig;
    
    public function __construct(
        BackgroundimageRepository $backgroundimageRepository,
        ImageComposerService $imageComposerService,
        DeleteForm $deleteForm,
        Config $devconfig,
        Config $fotoshooterconfig
    ) {
        $this->backgroundimageRepository = $backgroundimageRepository;
        $this->imageComposerService = $imageComposerService;
        $this->deleteForm = $deleteForm;
        $this->developmentconfig = $devconfig;
        $this->fotoshooterconfig = $fotoshooterconfig;
    }
    
    public function shootAction()
    {
        $picturename = $this->params()->fromRoute("picture");
        
        return new ViewModel(array(
            'picture' => $picturename
        ));
    }
    
    public function bgselectAction()
    {
        return new ViewModel(array(
            "columncount" => $this->fotoshooterconfig->get('column-quantity'),
            "bgimages" => $this->backgroundimageRepository->getCollection(),
        ));
    }
    
    public function composeAction()
    {
        $picturename = $this->params()->fromRoute("id");

        $backgroundImage = $this->backgroundimageRepository->getBackground($picturename);
        $file = $backgroundImage->getImageurl() . "/" . $backgroundImage->getImagename();

        $result = $this->imageComposerService->composeImage($file);
        $this->redirect()->toRoute("showimage");
        return new ViewModel();
    }
    
    public function showAction()
    {
        if ($this->request->isPost()) {
            $yesButtonValue = $this->params()->fromPost("yesbutton");
            if (!is_null($yesButtonValue)) {
                $result = $this->imageComposerService->saveImage();
                //@todo $result loggen
                
                $this->redirect()->toRoute('imagesaved');
            } else {
                $result = $this->imageComposerService->deleteImage();
                //@todo $result loggen;
                
                $this->redirect()->toRoute('home');
            }
        }
        $deleteForm = $this->deleteForm;
        
        return new ViewModel(array(
            'deleteForm' => $deleteForm,
        ));
    }
    
    public function savedAction()
    {
        $redirector = $this->redirect();
        return new ViewModel(array(
            'redirector' => $redirector,
        ));
    }
    
    public function waitAction()
    {
        $developmentEnabled = $this->developmentconfig->get('enable');
        $returnValue = 0;
        $picturename = $this->params()->fromRoute("picture");
        
        
        if ($developmentEnabled) {
            $copied = copy(
                FOTOSHOOTER_ROOT . '/src/Fotoshooter/External/devimage.jpg',
                ROOT_PATH . '/test.jpg'
            );
            chmod(ROOT_PATH . '/test.jpg', 0777);
            $returnValue = ($copied ? 0 : 1);
        } else {
            system(
                "cd " . FOTOSHOOTER_ROOT . "/src/Fotoshooter/External; sudo -S " . FOTOSHOOTER_ROOT . "/src/Fotoshooter/External/cameraExecuter 13",
                $returnValue
            );
        }

            

        if ($returnValue === 0) {
            rename(ROOT_PATH . "/test.jpg", FOTOSHOOTER_ROOT . "/src/Fotoshooter/External/test.jpg");
            
            $url = '/background/compose/' . $picturename;
            return new ViewModel(array(
                'picturename' => $picturename
            ));
        } else {
            $this->redirect()->toRoute('home');
        }
    }
}
