#!bin/bash
cd  $(dirname "$0")

sudo -S convert test.jpg -monitor -resize '1024x768' test.png
sudo -S convert -monitor ./test.png -background none -transparent white -channel Alpha -fx '1-((g-r)+(g-b)+(g-(r+b)/2))^2' -channel Green -fx '(r+b)/2' out.png
