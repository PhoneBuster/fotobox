<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Fotoshooter\Model\Entity;

use Equipment\Model\Entity\AbstractEntity;

/**
 * Description of BackgroundEntity
 *
 * @author cparadies
 */
class BackgroundEntity extends AbstractEntity
{
    private $imageurl;
    private $imagename;
    private $imagesize;
 
    public function __construct($imageurl, $imagename, $imagesize)
    {
        $this->setImageurl($imageurl);
        $this->setImagename($imagename);
        $this->setImagesize($imagesize);
    }
    public function getImageurl()
    {
        return $this->imageurl;
    }

    public function getImagename($withPath = true)
    {
        if ($withPath == true) {
            return $this->imagename;
        }
        
        $parts = explode("/", $this->imagename);
        $quantityOfParts = count($parts);
        
        return $parts[$quantityOfParts - 1];
    }

    public function getWebLink()
    {
        $publicPosInImageName = strpos($this->getImagename(), "public");
        
        if ($publicPosInImageName === false) {
            return $this->getImagename();
        }
        
        return substr($this->getImagename(), $publicPosInImageName + 6);
    }
    
    public function getImagesize()
    {
        return $this->imagesize;
    }

    public function setImageurl($imageurl)
    {
        $this->imageurl = $imageurl;
    }

    public function setImagename($imagename)
    {
        $this->imagename = $imagename;
    }

    public function setImagesize($imagesize)
    {
        $this->imagesize = $imagesize;
    }
}
