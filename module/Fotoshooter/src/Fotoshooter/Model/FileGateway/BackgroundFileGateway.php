<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Fotoshooter\Model\FileGateway;

use Fotoshooter\Model\Entity\BackgroundEntity;
use Fotoshooter\Model\FileGateway\Interfaces\BackgroundFileGatewayInterface;
use Fotoshooter\Service\FileInfoService;

/**
 * Description of BackgroundGateway
 *
 * @author cparadies
 */
class BackgroundFileGateway implements BackgroundFileGatewayInterface
{
    /**
     * @var string
     */
    private $backgroundImagePath = null;
    /**
     * @var FileInfoService
     */
    private $fileInfoService;
    
    public function __construct($backgroundImagePath, FileInfoService $fileInfoService)
    {
        $this->backgroundImagePath = $backgroundImagePath;
        $this->fileInfoService = $fileInfoService;
    }

    /*
     * {@inheritdoc}
     */
    public function fetchCollection()
    {
        $this->fileInfoService->setPathToRead($this->backgroundImagePath);
        $backgroundImageCollection = $this->fileInfoService->getAllFilesFromPath();
        
        return $backgroundImageCollection;
    }
    
    public function fetchBackground($backgroundName)
    {
        $this->fileInfoService->setPathToRead($this->backgroundImagePath);
        $backgroundImage = $this->fileInfoService->getFileFromPath($backgroundName);

        return new BackgroundEntity(
            $backgroundImage['path'],
            $backgroundImage['filename'],
            $backgroundImage['filesize']
        );
    }
}
