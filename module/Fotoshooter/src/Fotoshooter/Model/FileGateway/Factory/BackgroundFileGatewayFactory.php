<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Fotoshooter\Model\FileGateway\Factory;

use Fotoshooter\Model\FileGateway\BackgroundFileGateway;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Description of BackgroundFileGatewayFactory
 *
 * @author cparadies
 */
class BackgroundFileGatewayFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $backgroundImagePath = ROOT_PATH . "/public/img/background";
        $fileInfoService     = $serviceLocator->get(
            'Fotoshooter\Service\FileInfo'
        );
        
        $fileGateway = new BackgroundFileGateway($backgroundImagePath, $fileInfoService);
        
        return $fileGateway;
    }
}
