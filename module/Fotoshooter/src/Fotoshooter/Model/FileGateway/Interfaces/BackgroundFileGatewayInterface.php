<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Fotoshooter\Model\FileGateway\Interfaces;

/**
 * Description of BackgroundInterface
 *
 * @author cparadies
 */
interface BackgroundFileGatewayInterface
{
    /**
     * Returns a collection of background images
     *
     * @return array
     */
    public function fetchCollection();
    
    public function fetchBackground($backgroundName);
}
