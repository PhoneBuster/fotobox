<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Fotoshooter\Model\Form;

use Zend\Form\Element\Button;
use Zend\Form\Form;

/**
 * Description of DeleteForm
 *
 * @author cparadies
 */
class DeleteForm extends Form
{
    public function init()
    {
        parent::init();
        $this->addYesButton();
        $this->addNoButton();
    }
    
    private function addNoButton()
    {
        $button = new Button();
        $button->setAttribute("class", "btn btn-default btn-lg");
        $button->setAttribute("style", "margin: 5px;");
        $button->setAttribute("type", "submit");
        $button->setLabel("Nein");
        $button->setName("nobutton");
        $button->setValue("no");
        
        $this->add($button);
    }
    
    private function addYesButton()
    {
        $button = new Button();
        
        $button->setAttribute("class", "btn btn-success btn-lg");
        $button->setAttribute("style", "margin: 5px;");
        $button->setAttribute("type", "submit");
        $button->setLabel("Ja");
        $button->setName("yesbutton");
        $button->setValue("yes");
        $this->add($button);
    }
}
