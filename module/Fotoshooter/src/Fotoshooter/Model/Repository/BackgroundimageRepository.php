<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Fotoshooter\Model\Repository;

use Fotoshooter\Model\FileGateway\BackgroundFileGateway;

/**
 * Description of BackgroundimageRepository
 *
 * @author cparadies
 */
class BackgroundimageRepository
{
    /**
     * @var BackgroundFileGateway
     */
    private $backgroundGateway;
    
    public function __construct($backgroundGateway)
    {
        $this->backgroundGateway = $backgroundGateway;
    }
    
    public function getCollection()
    {
        return $this->backgroundGateway->fetchCollection();
    }
    
    public function getBackground($backgroundName)
    {
        return $this->backgroundGateway->fetchBackground($backgroundName);
    }
}
