<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Fotoshooter\Model\Repository\Factory;

use Fotoshooter\Model\Repository\BackgroundimageRepository;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Description of BackgroundimageRepositoryFactory
 *
 * @author cparadies
 */
class BackgroundimageRepositoryFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $backgroundImageFileGateway = $serviceLocator->get(
            'Fotoshooter\FileGateway\Background'
        );
        
        $repository = new BackgroundimageRepository($backgroundImageFileGateway);
        
        return $repository;
    }
}
