<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Fotoshooter\Model\Repository\Interfaces;

/**
 * Description of BackgroundimageRepositoryInterface
 *
 * @author cparadies
 */
interface BackgroundimageRepositoryInterface
{
    /**
     * Returns a collection of backgroundimages
     *
     * @return array
     */
    public function getCollection();
    
    public function getBackground($backgroundName);
}
