<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Fotoshooter\Service;

use Equipment\Application\Result;
use Equipment\Application\Service\Interfaces\CommandInterface;

/**
 * Description of DeleteComposedImageCommand
 *
 * @author cparadies
 */
class DeleteComposedImageCommand implements CommandInterface
{
    private $filename = "/ready.php";
    
    public function execute()
    {
        return $this->deleteImage();
    }
    
    private function deleteImage()
    {
        $path = ROOT_PATH . "/public/img";
        
        if (file_exists($path . $this->filename)) {
            $deleted = unlink($path . $this->filename);
            
            if ($deleted) {
                return $this->imageDeleted($path);
            } else {
                return $this->imageNotDeleted($path);
            }
        } else {
            return $this->imageNotDeleted($path);
        }
    }
    
    private function imageNotDeleted($path)
    {
        return Result::getResult(
            'Image could not be deleted!',
            Result::ERROR_MESSAGE,
            $path . $this->filename,
            false
        );
    }
    
    private function imageDeleted($path)
    {
        return Result::getResult(
            "Image was deleted!",
            Result::SUCCESS_MESSAGE,
            $path . $this->filename,
            true
        );
    }
    
    public function getFilename()
    {
        return $this->filename;
    }

    public function setFilename($filename)
    {
        if (substr($filename, 0, 1) != "/") {
            $this->filename = "/" . $filename;
        }
        
        $this->filename = $filename;
    }
}
