<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Fotoshooter\Service\Factory;

use Fotoshooter\Service\FileInfoService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Description of FileInfoServiceFactory
 *
 * @author cparadies
 */
class FileInfoServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $readAllFilesCommand = $serviceLocator->get(
            'Fotoshooter\Service\ReadAllFilesCommand'
        );
        $readFileSizecommand = $serviceLocator->get(
            'Fotoshooter\Service\GetFileSizeCommand'
        );
        $readFileCommand     = $serviceLocator->get(
            'Fotoshooter\Service\ReadFileCommand'
        );
        $fileService = new FileInfoService(
            $readAllFilesCommand,
            $readFileSizecommand,
            $readFileCommand
        );
        
        return $fileService;
    }
}
