<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Fotoshooter\Service\Factory;

use Fotoshooter\Service\ImageComposerService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Description of ImageComposerServiceFactory
 *
 * @author cparadies
 */
class ImageComposerServiceFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $saveComposedImageCommand = $serviceLocator->get('Fotoshooter\Service\SaveComposedImageCommand');
        $deleteComposedImageCommand = $serviceLocator->get('Fotoshooter\Service\DeleteComposedImageCommand');
        $imageComposerService = new ImageComposerService($saveComposedImageCommand, $deleteComposedImageCommand);
        
        return $imageComposerService;
    }
}
