<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Fotoshooter\Service\Factory;

use Fotoshooter\Service\ReadAllFilesCommand;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Description of ReadAllFilescommandFactory
 *
 * @author cparadies
 */
class ReadAllFilesCommandFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $command = new ReadAllFilesCommand();
        
        return $command;
    }
}
