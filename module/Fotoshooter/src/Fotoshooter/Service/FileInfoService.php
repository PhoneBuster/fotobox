<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Fotoshooter\Service;

use Fotoshooter\Model\Entity\BackgroundEntity;

/**
 * Description of FileInfoService
 *
 * @author cparadies
 */
class FileInfoService
{
    /**
     * @var string | null
     */
    private $pathToRead = null;
    /**
     * @var ReadAllFilesCommand
     */
    private $readAllFilesCommand;
    /**
     * @var GetFileSizeCommand
     */
    private $getFileSizeCommand;
    /**
     * @var ReadFileCommand;
     */
    private $readFileCommand;
    
    public function __construct(
        ReadAllFilesCommand $readAllFilesCommand,
        GetFileSizeCommand $getFileSizeCommand,
        ReadFileCommand $readFileCommand
    ) {
        $this->readAllFilesCommand = $readAllFilesCommand;
        $this->getFileSizeCommand  = $getFileSizeCommand;
        $this->readFileCommand     = $readFileCommand;
    }
    
    public function getAllFilesFromPath()
    {
        $allFiles = array();
        
        $this->readAllFilesCommand->setPathToRead($this->pathToRead);
       
        $resultAllFiles = $this->readAllFilesCommand->execute();
        
        foreach ($resultAllFiles->getData() as $file) {
            $this->getFileSizeCommand->setPathToFile("");
            $this->getFileSizeCommand->setFileName($file);
            
            $resultFileSize = $this->getFileSizeCommand->execute();
            
            $backgroundImage = new BackgroundEntity(
                $this->pathToRead,
                $file,
                $resultFileSize->getData()
            );

            $allFiles[] = $backgroundImage;
        }
        
        return $allFiles;
    }
    
    public function getFileFromPath($filename)
    {
        $this->readFileCommand->setFilename($filename);
        $this->readFileCommand->setPathToFile($this->pathToRead);
        $this->getFileSizeCommand->setPathToFile($this->pathToRead);
        $this->getFileSizeCommand->setFileName($filename);

        $resultFileSize = $this->getFileSizeCommand->execute();
        $result = $this->readFileCommand->execute();

        return array(
            "filename" => $filename,
            "path" => $this->pathToRead,
            "filesize" => $resultFileSize->getData(),
        );
    }
    
    public function setPathToRead($pathToRead)
    {
        $this->pathToRead = $pathToRead;
    }
}
