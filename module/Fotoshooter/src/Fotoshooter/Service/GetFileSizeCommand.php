<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Fotoshooter\Service;

use Equipment\Application\Result;
use Equipment\Application\Service\Interfaces\CommandInterface;
use Fotoshooter\Exception\NullPointerException;

/**
 * Description of GetFileSizeCommand
 *
 * @author cparadies
 */
class GetFileSizeCommand implements CommandInterface
{
    /**
     * @var string | null
     */
    private $pathToFile = null;
    /**
     * @var string | null
     */
    private $fileName = null;
    
    public function execute()
    {
        if (is_null($this->pathToFile) || is_null($this->fileName)) {
            throw new NullPointerException(
                'Path to file and / or fileName is null. Set it before executing this command'
            );
        }
        return $this->getFileSize();
    }
    
    private function getFileSize()
    {
        $file = ($this->pathToFile . $this->fileName);
        $fileSizeInByte = 0;
        
        if (file_exists($file)) {
            $fileSizeInByte = filesize($file);
        } else {
            echo "File : " . $file;
        }
        
        return Result::getResult(
            'Filesize',
            Result::INFO_MESSAGE,
            $fileSizeInByte,
            true
        );
    }
    
    public function setPathToFile($pathToFile)
    {
        $this->pathToFile = $pathToFile;
    }

    public function setFileName($fileName)
    {
        if (substr($fileName, 0, 1) != '/') {
            $this->fileName = '/' . $fileName;
        } else {
            $this->fileName = $fileName;
        }
    }
}
