<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Fotoshooter\Service;

/**
 * Description of ImageComposerService
 *
 * @author cparadies
 */
class ImageComposerService
{
    /**
     * @var SaveComposedImageCommand
     */
    private $saveComposedImageCommand;
    /**
     * @var $deleteComposedImageCommand
     */
    private $deleteComposedImageCommand;
    public function __construct(
        SaveComposedImageCommand $saveComposedImageCommand,
        DeleteComposedImageCommand $deleteComposedImageCommand
    ) {
        $this->saveComposedImageCommand   = $saveComposedImageCommand;
        $this->deleteComposedImageCommand = $deleteComposedImageCommand;
    }
    
    public function composeImage($filename)
    {
        $output = array();
        passthru("sh " . FOTOSHOOTER_ROOT . "/src/Fotoshooter/External/convertTestPng.sh", $output);
        if ($output === 0) {
            exec(
                "sh " . FOTOSHOOTER_ROOT . "/src/Fotoshooter/External/compositeimages.sh $filename",
                $output
            );
        } else {
            return false;
        }
        
        if ($output === 0) {
            return true;
        }
        
        return false;
    }
    
    public function deleteImage($filename = "/ready.png")
    {
        $this->deleteComposedImageCommand->setFilename($filename);
        return $this->deleteComposedImageCommand->execute();
    }
    
    public function saveImage($filename = "/ready.png")
    {
        $this->saveComposedImageCommand->setFilename($filename);
        return $this->saveComposedImageCommand->execute();
    }
}
