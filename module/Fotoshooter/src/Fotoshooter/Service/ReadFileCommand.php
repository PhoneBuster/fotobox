<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Fotoshooter\Service;

use Equipment\Application\Result;
use Equipment\Application\Service\Interfaces\CommandInterface;
use Fotoshooter\Exception\NullPointerException;

/**
 * Description of ReadFileCommand
 *
 * @author cparadies
 */
class ReadFileCommand implements CommandInterface
{
    private $filename = null;
    private $pathToFile = null;
    
    public function execute()
    {
        if (is_null($this->filename) || is_null($this->pathToFile)) {
            throw new NullPointerException(
                'Filename and/or pathToFile is/are null. Set it before executing this command'
            );
        }
        
        return $this->getFile();
    }
    
    
    public function getFile()
    {
        $file = $this->pathToFile . $this->filename;

        if (file_exists($file)) {
            return Result::getResult(
                'File exists',
                Result::SUCCESS_MESSAGE,
                $file,
                true
            );
        }
        
        return Result::getResult(
            'File not exists',
            Result::WARNING_MESSAGE,
            $file,
            false
        );
    }
    
    public function setFilename($filename)
    {
        if (substr($filename, 0, 1) != '/') {
            $this->filename = '/' . $filename;
        } else {
            $this->filename = $filename;
        }
    }
    
    public function setPathToFile($pathToFile)
    {
        $this->pathToFile = $pathToFile;
    }
}
