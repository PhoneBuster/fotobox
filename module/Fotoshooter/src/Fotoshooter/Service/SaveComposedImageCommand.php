<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Fotoshooter\Service;

use DateTime;
use Equipment\Application\Result;
use Equipment\Application\Service\Interfaces\CommandInterface;

/**
 * Description of SaveComposedImageCommand
 *
 * @author cparadies
 */
class SaveComposedImageCommand implements CommandInterface
{
    /**
     * @var string
     */
    private $filename = "/ready.png";
    
    public function execute()
    {
        return $this->saveImage();
    }
    
    private function saveImage()
    {
        $path = ROOT_PATH . "/public/img";
        
        if (file_exists($path . $this->filename)) {
            $now = new DateTime();
            $unixtimestamp = $now->getTimestamp();
            
            $saved = rename($path . $this->filename, $path . "/Fotos/" . strval($unixtimestamp) . ".png");
            
            if ($saved) {
                return $this->imageSaved($path, $unixtimestamp);
            } else {
                return $this->imageNotSaved($path, $unixtimestamp);
            }
        } else {
            return $this->imageNotSaved($path, $unixtimestamp);
        }
    }
    
    private function imageNotSaved($path, $unixtimestamp)
    {
        return Result::getResult(
            'Image could not be saved!',
            Result::ERROR_MESSAGE,
            $path . "/Fotos/" . strval($unixtimestamp) . ".png",
            false
        );
    }
    
    private function imageSaved($path, $unixtimestamp)
    {
        return Result::getResult(
            "Image was saved!",
            Result::SUCCESS_MESSAGE,
            $path . "/Fotos/" . strval($unixtimestamp) . ".png",
            true
        );
    }
    
    public function getFilename()
    {
        return $this->filename;
    }

    public function setFilename($filename)
    {
        if (substr($filename, 0, 1) != "/") {
            $this->filename = "/" . $filename;
        }
        
        $this->filename = $filename;
    }
}
