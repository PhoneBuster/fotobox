<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Fotoshooter\View\Helper;

use Fotoshooter\Model\Entity\BackgroundEntity;
use Zend\View\Helper\AbstractHelper;

/**
 * Description of BackgroundGallery
 *
 * @author cparadies
 */
class BackgroundGallery extends AbstractHelper
{
    private $columncount;
    
    public function __invoke(array $backgrounds, $columncount)
    {
        $quantity          = count($backgrounds);
        $this->columncount = $columncount;
        
        $this->showBackgrounds($backgrounds, $quantity);

    }
    
    private function showBackgrounds(array $backgrounds, $quantity)
    {
        $counter = 0;
        
        echo '<div class="container text-center">';
        /* @var $background BackgroundEntity */
        foreach ($backgrounds as $background) {
            if (($counter % $this->columncount) == 0) {
                echo '<br>';
            }
            echo '  <a href="/shoot/' . $background->getImagename(false) . '">';
            echo '  <img onclick="showLoading()" style="padding-bottom: 4px;" src="' . $background->getWebLink() .  '" width="254" height="190">';
            echo '  </a>';
            $counter++;
        }
        echo '</div>';
    }
}
