<?php

return array(
    'router' => array(
        'routes' => array(
            'pictures' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => '/pictures[/:page]',
                    'defaults' => array(
                        'constraints' => '[0-9]*',
                        'controller' => 'Imagegallery\Controller\Gallery',
                        'action' => 'show',
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'factories' => array(
            'Imagegallery\Controller\Gallery' => 'Imagegallery\Controller\Factory\GalleryControllerFactory',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map'              => include __DIR__  .'/../template_map.php',
        'template_path_stack'       => array(
            __DIR__ . '/../view',
        ),
    ),
    'view_helpers'    => array(
        'invokables' => array(
            'ImageGallery' => 'Imagegallery\View\Helper\ImageGallery',
        ),
    ),
);
