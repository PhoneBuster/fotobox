<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Imagegallery\Controller\Factory;

use Imagegallery\Controller\GalleryController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Description of GalleryControllerFactory
 *
 * @author cparadies
 */
class GalleryControllerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $controllerManager)
    {
        $serviceLocator = $controllerManager->getServiceLocator();
        
        $fileService    = $serviceLocator->get('Equipment\Service\FileService');
        $controller = new GalleryController($fileService);
        
        return $controller;
    }
}
