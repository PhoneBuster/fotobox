<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Imagegallery\Controller;

use Equipment\Application\Service\FileService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Description of GalleryController
 *
 * @author cparadies
 */
class GalleryController extends AbstractActionController
{
    /**
     * @var FileService
     */
    private $fileServce;
    
    public function __construct(FileService $fileService)
    {
        $this->fileServce = $fileService;
    }
    
    public function showAction()
    {
        $page = 1;
        
        if (!is_null($this->params('page'))) {
            $page = intval($this->params('page'));
        }

        $images = $this->fileServce->getAllFilesFromFolder(ROOT_PATH . '/public/img/Fotos')->getData();

        return new \Zend\View\Model\ViewModel(array(
            'images' => $images,
            'page' => $page,
        ));
    }
}
