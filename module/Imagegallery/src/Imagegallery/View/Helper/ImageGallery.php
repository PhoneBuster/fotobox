<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Imagegallery\View\Helper;

use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Paginator\Paginator;
use Zend\View\Helper\AbstractHelper;

/**
 * Description of ImageGallery
 *
 * @author cparadies
 */
class ImageGallery extends AbstractHelper
{
    /**
     * @var Paginator
     */
    private $paginator;
    
    public function __invoke(array $images, $page)
    {
        $arrayAdapter = new ArrayAdapter($images);
        $this->paginator = new Paginator($arrayAdapter);
        
        $this->configurePaginator($page);
        $this->showImages();
        $this->printPageNavigation();
        $this->preparePictureDiv();
    }
    
    private function showImages()
    {
        $counter = 0;
        $pages = $this->paginator->getPages();

        if (($pages->currentItemCount) > 0) {
            echo '<div class="container center-text">';
            foreach ($this->paginator as $item) {
                if (($counter % 3) == 0) {
                    echo '<br>';
                }
                $img = explode('/', $item);
                $last = count($img) - 1;
                $item = '/img/Fotos/' . $img[$last];
                echo '<img src="' . $item . '" class="size_xs" onclick="showpicture(\'' . $item . '\')">';
                $counter++;
            }
            echo "</div>";
        }
    }
    
    private function printPageNavigation()
    {
        $pages = $this->paginator->getPages();
        $currentPageNumber = $this->paginator->getCurrentPageNumber();
        
        echo '<div class="container">';
        echo '<center>';
        for ($i = 1; $i <= $pages->pageCount; $i++) {
            if ($i == $currentPageNumber) {
                echo '<a class="btn btn-lg current_site" href="#">' . $i . '</a>';
            } else {
                echo '<a class="btn btn-lg other_sites" href="/pictures/' . $i . '">' . $i . '</a>';
            }
        }
        echo '<a class="btn btn-lg btn-greate-blue" href="/">Back</a>';
        echo '</center>';
        echo '</div>';
    }
    
    private function configurePaginator($pageNumber)
    {
        $this->getView()->getEngine();
        $this->paginator->setCurrentPageNumber($pageNumber);
        $this->paginator->setDefaultScrollingStyle();
        $this->paginator->setItemCountPerPage(9);
    }
    
    private function preparePictureDiv()
    {
        echo '<div id="picturediv" class="picture-733">';
        echo '  <img id="bigimage" class="image-733" src="" onclick="hidepicture()">';
        echo '</div>';
    }
}
